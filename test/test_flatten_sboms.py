import test

from pathlib import Path
from unittest import TestCase, mock

from hoppr.configs.manifest import Manifest
from hoppr.flatten_sboms import flatten_sboms


class TestFlattenSboms(TestCase):
    def test_flatten_sbom_success(self):
        Manifest.loaded_manifests.clear()

        manifest_file = Path(test.__file__).parent / "resources" / "manifest" / "unit" / "manifest.yml"
        manifest = Manifest.load_file(manifest_file)

        flattened_sbom = flatten_sboms(manifest)

        self.assertEqual(len(flattened_sbom.components), 5)

        self.assertEqual(flattened_sbom.components[0].purl, "pkg:npm/@angular-devkit/architect@0.1303.1")
        self.assertEqual(flattened_sbom.components[1].purl, "pkg:npm/@create-react-app/web-app@0.7624.1")
        self.assertEqual(flattened_sbom.components[2].purl, "pkg:pypi/wsgiref@0.1.2")
        self.assertEqual(flattened_sbom.components[3].purl, "pkg:pypi/argparse@1.2.1")
        self.assertEqual(flattened_sbom.components[4].purl, "pkg:npm/@webpack/block@0.9853.2")

        self.assertEqual(len(flattened_sbom.components[0].externalReferences), 2)
        self.assertEqual(len(flattened_sbom.components[1].externalReferences), 1)
        self.assertEqual(len(flattened_sbom.components[2].externalReferences), 2)
        self.assertEqual(len(flattened_sbom.components[3].externalReferences), 1)
        self.assertEqual(len(flattened_sbom.components[4].externalReferences), 1)

        self.assertEqual(flattened_sbom.components[0].externalReferences[0].url, "../../bom/unit_bom1_mini.json")
        self.assertEqual(flattened_sbom.components[0].externalReferences[1].url, "../../bom/unit_bom3_mini.json")
        self.assertEqual(flattened_sbom.components[1].externalReferences[0].url, "../../bom/unit_bom1_mini.json")
        self.assertEqual(flattened_sbom.components[2].externalReferences[0].url, "../../bom/unit_bom2_mini.json")
        self.assertEqual(flattened_sbom.components[2].externalReferences[1].url, "../../bom/unit_bom3_mini.json")
        self.assertEqual(flattened_sbom.components[3].externalReferences[0].url, "../../bom/unit_bom2_mini.json")
        self.assertEqual(flattened_sbom.components[4].externalReferences[0].url, "../../bom/unit_bom3_mini.json")

        self.assertEqual(len(flattened_sbom.components[0].properties), 1)
        self.assertEqual(len(flattened_sbom.components[1].properties), 1)
        self.assertEqual(len(flattened_sbom.components[2].properties), 3)
        self.assertEqual(len(flattened_sbom.components[3].properties), 3)
        self.assertEqual(len(flattened_sbom.components[4].properties), 1)

    sbom_content = {
        "bomFormat": "CycloneDX",
        "specVersion": "1.4",
        "serialNumber": "urn:uuid:79190df2-cebf-46d1-b651-681b8b7784e3",
        "version": 1,
        "components": [
            {
                "type": "library",
                "author": "Angular Authors",
                "name": "@angular-devkit/architect",
                "version": "0.1303.1",
                "purl": "pkg:npm/@angular-devkit/architect@0.1303.1",
            }
        ],
    }

    @mock.patch("hoppr.net.load_url", return_value=sbom_content)
    def test_flatten_sbom_by_url_success(self, mock_sbom):
        manifest_file = Path("test", "resources", "manifest", "unit", "manifest-url-sbom.yml")
        manifest = Manifest.load_file(manifest_file)
        sbom = flatten_sboms(manifest)

        self.assertEqual(sbom.components[0].purl, "pkg:npm/@angular-devkit/architect@0.1303.1")
        self.assertEqual(len(sbom.components), 1)
