import unittest

from hoppr.hoppr_types.component_coverage import ComponentCoverage


class TestComponentCoverage(unittest.TestCase):
    def test_accepts_count(self):
        assert ComponentCoverage.OPTIONAL.accepts_count(0), "OPTIONAL should accept a count of 0"
        assert ComponentCoverage.OPTIONAL.accepts_count(1), "OPTIONAL should accept a count of 1"
        assert ComponentCoverage.OPTIONAL.accepts_count(99), "OPTIONAL should accept a count of 99"
        assert not ComponentCoverage.EXACTLY_ONCE.accepts_count(0), "EXACTLY_ONCE should not accept a count of 0"
        assert ComponentCoverage.EXACTLY_ONCE.accepts_count(1), "EXACTLY_ONCE should accept a count of 1"
        assert not ComponentCoverage.EXACTLY_ONCE.accepts_count(2), "EXACTLY_ONCE should not accept a count of 2"
        assert not ComponentCoverage.AT_LEAST_ONCE.accepts_count(0), "AT_LEAST_ONCE should not accept a count of 0"
        assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(1), "AT_LEAST_ONCE should accept a count of 1"
        assert ComponentCoverage.AT_LEAST_ONCE.accepts_count(7), "AT_LEAST_ONCE should accept a count of 7"
        assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(0), "NO_MORE_THAN_ONCE should accept a count of 0"
        assert ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(1), "NO_MORE_THAN_ONCE should accept a count of 1"
        assert not ComponentCoverage.NO_MORE_THAN_ONCE.accepts_count(7), "NO_MORE_THAN_ONCE should not accept a count of 7"
