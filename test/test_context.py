import multiprocessing

from hoppr.context import Context


def test_context():
    context = Context(
        manifest="MANIFEST",
        collect_root_dir="my_root",
        consolidated_sbom="BOM",
        delivered_sbom="BOM",
        max_processes=3,
        logfile_lock=multiprocessing.Manager().RLock(),
    )

    assert context.collect_root_dir == "my_root"
    assert context.manifest == "MANIFEST"
    assert context.consolidated_sbom == "BOM"
    assert context.max_attempts == 3
