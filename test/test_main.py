import unittest

from pathlib import Path
from unittest.mock import patch

import pytest

from click.exceptions import Exit

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from hoppr_cyclonedx_models.cyclonedx_1_4 import (
    CyclonedxSoftwareBillOfMaterialsStandard as Bom,
)

import hoppr.main

from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.processor import HopprProcessor
from hoppr.result import Result

test_component_list = [
    Component(
        name="transfer-test",
        type="file",
        purl="pkg:generic/README.md",
    ),
    Component(
        name="manifest",
        type="file",
        purl="pkg:generic/docs/CHANGELOG.md",
    ),
]

test_bom = Bom(specVersion="1.4", version=1, bomFormat="CycloneDX")
test_bom.components = test_component_list


class TestMain(unittest.TestCase):
    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_success(
        self,
        mock_transfer_load,
        mock_cred_load,
        mock_manifest_load,
        mock_copyfile,
        mock_flatten_sboms,
    ):
        hoppr.main.bundle(
            "mock_manifest_file",
            "mock_creds_file",
            "mock_transfer_file",
            Path("mylog.txt"),
        )

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_no_key(
        self,
        mock_transfer_load,
        mock_cred_load,
        mock_manifest_load,
        mock_copyfile,
        mock_flatten_sboms,
    ):
        with pytest.raises(Exit) as pytest_wrapped_e:
            hoppr.main.bundle(
                manifest_file="mock_manifest_file",
                credentials_file="mock_creds_file",
                transfer_file="mock_transfer_file",
                log_file=Path("mylog.txt"),
                create_attestations=True,
            )
        assert pytest_wrapped_e.type == Exit

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    @patch('hoppr.main.prompt', return_value="5678")
    @patch('hoppr.main.HopprProcessor')
    def test_bundle_attest_no_prompt(
        self,
        mock_hoppr_processor,
        mock_prompt,
        mock_transfer_load,
        mock_cred_load,
        mock_manifest_load,
        mock_copyfile,
        mock_flatten_sboms,
    ):
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                manifest_file="mock_manifest_file",
                credentials_file="mock_creds_file",
                transfer_file="mock_transfer_file",
                log_file=Path("mylog.txt"),
                create_attestations=True,
                functionary_key_path=Path("mylog.txt"),
            )
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1
        mock_prompt.assert_not_called()
        mock_hoppr_processor.assert_called()

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    @patch('hoppr.main.prompt', return_value="5678")
    @patch('hoppr.main.HopprProcessor')
    def test_bundle_attest_prompt(
        self,
        mock_hoppr_processor,
        mock_prompt,
        mock_transfer_load,
        mock_cred_load,
        mock_manifest_load,
        mock_copyfile,
        mock_flatten_sboms,
    ):
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                manifest_file="mock_manifest_file",
                credentials_file="mock_creds_file",
                transfer_file="mock_transfer_file",
                log_file=Path("mylog.txt"),
                create_attestations=True,
                functionary_key_path=Path("mylog.txt"),
                functionary_key_prompt=True,
            )
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1
        mock_prompt.assert_called()
        mock_hoppr_processor.assert_called()

    @patch("shutil.copyfile")
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_fail_no_bom(
        self, mock_transfer_load, mock_cred_load, mock_manifest_load, mock_copyfile
    ):
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                "mock_manifest_file", "mock_creds_file", "mock_transfer_file", None
            )
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    @patch.object(HopprProcessor, "run", return_value=Result.fail("oops"))
    @patch.object(Manifest, "load_file")
    @patch.object(Credentials, "load_file")
    @patch.object(Transfer, "load_file")
    def test_bundle_fail(
        self, mock_load_file, mock_cred_load, mock_manifest_load, mock_process_run
    ):
        with pytest.raises(SystemExit) as pytest_wrapped_e:
            hoppr.main.bundle(
                "mock_manifest_file", "mock_creds_file", "mock_transfer_file", None
            )
        assert pytest_wrapped_e.type == SystemExit
        assert pytest_wrapped_e.value.code == 1

    @patch.object(Transfer, "load_file")
    @patch('hoppr.main.generate_in_toto_layout')
    @patch('hoppr.main.prompt', return_value="5678")
    def test_generate_layout_no_prompt(
        self,
        mock_prompt,
        mock_generate_in_toto_layout,
        mock_transfer_load
    ):
        product_owner_path = Path("product_owner_key")
        functionary_path = Path("functionary_key")

        hoppr.main.generate_layout(
            "mock_transfer_file",
            product_owner_path,
            functionary_path,
            False,
            "1234",
        )
        mock_prompt.assert_not_called()
        mock_generate_in_toto_layout.assert_called_with(mock_transfer_load(), product_owner_path, functionary_path, "1234")

    @patch.object(Transfer, "load_file")
    @patch('hoppr.main.generate_in_toto_layout')
    @patch('hoppr.main.prompt', return_value="5678")
    def test_generate_layout_prompt(
        self,
        mock_prompt,
        mock_generate_in_toto_layout,
        mock_transfer_load
    ):
        product_owner_path = Path("product_owner_key")
        functionary_path = Path("functionary_key")

        hoppr.main.generate_layout(
            "mock_transfer_file",
            product_owner_path,
            functionary_path,
            True,
            "1234",
        )
        mock_prompt.assert_called()
        mock_generate_in_toto_layout.assert_called_with(mock_transfer_load(), product_owner_path, functionary_path, "5678")

    def test_validate(self):
        hoppr.main.validate(
            [Path("test", "resources", "manifest", "unit", "manifest.yml").resolve()],
            Path("test", "resources", "credential", "cred-test.yml").resolve(),
            Path("test", "resources", "transfer", "transfer-test.yml").resolve(),
        )
