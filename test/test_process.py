import multiprocessing
import os
import platform

from concurrent.futures import Future
from pathlib import Path, PosixPath
from unittest import TestCase
from unittest.mock import patch

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from hoppr_cyclonedx_models.cyclonedx_1_4 import (
    CyclonedxSoftwareBillOfMaterialsStandard as Bom,
)

import hoppr.main

from hoppr import utils
from hoppr.base_plugins.hoppr import HopprPlugin, hoppr_process
from hoppr.configs.credentials import Credentials
from hoppr.configs.manifest import Manifest
from hoppr.configs.transfer import Transfer
from hoppr.context import Context
from hoppr.exceptions import HopprPluginError
from hoppr.hoppr_types.bom_access import BomAccess
from hoppr.hoppr_types.component_coverage import ComponentCoverage
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.transfer_file_content import Plugin
from hoppr.hoppr_types.transfer_file_content import Stage as StageRef
from hoppr.mem_logger import MemoryLogger
from hoppr.processor import HopprProcessor, StageProcessor
from hoppr.result import Result

test_component_list = [
    Component(
        name="transfer-test",
        type="file",
        purl="pkg:generic/README.md",
    ),
    Component(
        name="manifest",
        type="file",
        purl="pkg:generic/docs/CHANGELOG.md",
    ),
]

test_bom = Bom(specVersion="1.4", version=1, bomFormat="CycloneDX")
test_bom.components = test_component_list


class UnitTestPlugin(HopprPlugin):

    test_return_obj = None

    def get_version(self) -> str:
        return "0.1.2"

    @hoppr_process
    def pre_stage_process(self):
        print(f"Hiya World")
        self.get_logger().info(f"Hiya World")
        return Result.success(return_obj=self.test_return_obj)

    @hoppr_process
    def post_stage_process(self):
        print(f"Ta-ta World")
        self.get_logger().info(f"Ta-ta World")
        return Result.retry("try again")


class SecondPlugin(UnitTestPlugin):
    pass


class TestHopprProcess(TestCase):
    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch.object(HopprProcessor, '_collect_file')
    @patch.object(StageProcessor, 'run', return_value=Result.success())
    @patch.object(Manifest, 'load_file')
    def test_run_success(self, mock_manifest_load, mock_stage_run, mock_collect_file, mock_flatten_sbom):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        proc = HopprProcessor(transfer, manifest)
        proc.metadata_files = ["dummy_metadata"]

        result = proc.run()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch.object(os, "getlogin", return_value="Fake User")
    @patch.object(Path, attribute="__new__", return_value=PosixPath.cwd().joinpath("generic", "_metadata_"))
    @patch("hoppr.processor.os.name", "nt")
    @patch.object(HopprProcessor, "_collect_file")
    @patch.object(StageProcessor, "run", return_value=Result.success())
    @patch.object(Transfer, "load_file", return_value=Transfer())
    @patch.object(Manifest, "load_file", return_value=Manifest("C:\\TEMP\\test-manifest.yml"))
    def test_run_success_platform_windows(
        self,
        mock_manifest_load,
        mock_transfer_load,
        mock_stage_run,
        mock_collect_file,
        mock_windows_path,
        mock_oslogin,
        mock_flatten_sboms,
    ):
        proc = HopprProcessor(mock_transfer_load, mock_manifest_load)
        proc.metadata_files = ["dummy_metadata"]

        result = proc.run()
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

    @patch("hoppr.processor.flatten_sboms", return_value=test_bom)
    @patch.object(HopprProcessor, '_summarize_results', return_value=42)
    @patch.object(StageProcessor, 'run', return_value=Result.fail("oops"))
    @patch.object(Manifest, 'load_file')
    def test_run_fail(self, mock_manifest_load, mock_stage_run, mock_summarize_results, mock_flatten_sboms):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        proc = HopprProcessor(transfer, manifest)
        result = proc.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"

        assert proc.get_logger() is not None

    @patch.object(Manifest, 'load_file')
    def test_summary_success(self, mock_manifest_load, ):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.results["pre_stage_process"] = {
            ("plugin-a", None, Result.success()),
            ("plugin-a", None, Result.fail()),
            ("plugin-a", None, Result.retry()),
        }
        stage.results["process_component"] = {
            ("plugin-a", "my-component", Result.success()),
            ("plugin-a", "my-component", Result.fail()),
            ("plugin-a", "my-component", Result.retry()),
        }
        proc.stages["test-stage"] = stage

        failure_count = proc._summarize_results()

        assert failure_count == 4


class TestStageProcess(TestCase):
    def test_run_plugin(self):
        context = Context(
            manifest=None,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        plugin = UnitTestPlugin(context)

        result = hoppr.processor._run_plugin(plugin, "pre_stage_process", None)
        assert result.is_success(), f"Expected SUCCESS result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "process_component", None)
        assert result.is_skip(), f"Expected SKIP result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "post_stage_process", None)
        assert result.is_retry(), f"Expected RETRY result, got {result}"

        result = hoppr.processor._run_plugin(plugin, "not_a_real_process", None)
        assert result.is_fail(), f"Expected FAIL result, got {result}"

        plugin.bom_access = BomAccess.COMPONENT_ACCESS
        plugin.test_return_obj = "String is not good"
        result = hoppr.processor._run_plugin(plugin, "pre_stage_process", None)
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Plugin UnitTestPlugin has BOM access level COMPONENT_ACCESS, but returned an object of type str"

        plugin.bom_access = BomAccess.FULL_ACCESS
        plugin.test_return_obj = "String is not good"
        result = hoppr.processor._run_plugin(plugin, "pre_stage_process", None)
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Plugin UnitTestPlugin has BOM access level FULL_ACCESS, but returned an object of type str"

        plugin.bom_access = BomAccess.NO_ACCESS
        plugin.test_return_obj = "String is not good"
        result = hoppr.processor._run_plugin(plugin, "pre_stage_process", None)
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Plugin UnitTestPlugin has BOM access level NO_ACCESS, but returned an object of type str"

    def test_get_req_coverage(self):
        context = Context(
            manifest=None,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        stage = StageProcessor(StageRef("test-stage", {}), context)
        assert stage._get_required_coverage() == ComponentCoverage.OPTIONAL

        p1 = UnitTestPlugin(
            Context(
                Manifest(),
                "ROOT",
                None,
                None,
                3,
                logfile_lock=multiprocessing.Manager().RLock(),
            )
        )
        p1.default_component_coverage = ComponentCoverage.NO_MORE_THAN_ONCE
        p2 = UnitTestPlugin(
            Context(
                Manifest(),
                "ROOT",
                None,
                None,
                3,
                logfile_lock=multiprocessing.Manager().RLock(),
            )
        )

        stage.plugins = [p1, p2]
        with pytest.raises(HopprPluginError):
            stage._get_required_coverage()

        stage = StageProcessor(StageRef("test-stage", {"component_coverage": "AT_LEAST_ONCE"}), context)
        assert stage._get_required_coverage() == ComponentCoverage.AT_LEAST_ONCE

    def test_check_component_coverage(self):
        context = Context(
            manifest=None,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        stage = StageProcessor(StageRef("test-stage", {"component_coverage": "EXACTLY_ONCE"}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.required_coverage = stage._get_required_coverage()

        stage._save_result("process_component", "TestPlugin", Result.success(), stage.context.delivered_sbom.components[0])

        assert stage._check_component_coverage("process_component") == 1
        assert len(stage.results["process_component"]) == 2

    def test_update_bom(self):
        context = Context(
            manifest=None,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        stage = StageProcessor(StageRef("test-stage", {"component_coverage": "EXACTLY_ONCE"}), context)
        stage.context.delivered_sbom.components = test_component_list

        new_bom = Bom(specVersion="1.4", version=1, bomFormat="CycloneDX")

        new_comp = Component(
            name="transfer-test",
            type="file",
            purl="pkg:generic/MODIFIED_README.md",
        )

        new_bom.components = [new_comp]

        stage._update_bom(new_bom, None)

        assert len(stage.context.delivered_sbom.components) == 1
        assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"

    def test_update_bom_component(self):
        context = Context(
            manifest=None,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        stage = StageProcessor(StageRef("test-stage", {"component_coverage": "EXACTLY_ONCE"}), context)
        stage.context.delivered_sbom.components = test_component_list

        new_comp = Component(
            name="transfer-test",
            type="file",
            purl="pkg:generic/MODIFIED_README.md",
        )

        stage._update_bom(new_comp, test_component_list[0])

        assert stage.context.delivered_sbom.components[0].purl == "pkg:generic/MODIFIED_README.md"

    @patch(
        'hoppr.processor.plugin_instance',
        side_effect=[
            UnitTestPlugin(
                Context(
                    Manifest(),
                    "ROOT",
                    None,
                    None,
                    3,
                    logfile_lock=multiprocessing.Manager().RLock(),
                )
            ),
            SecondPlugin(
                Context(
                    Manifest(),
                    "ROOT",
                    None,
                    None,
                    3,
                    logfile_lock=multiprocessing.Manager().RLock(),
                )
            ),
        ],
    )
    @patch.object(Manifest, 'load_file')
    def test_check_bom_access_full(self, mock_manifest_load, mock_plugin_instance):
        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None),
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        stage.plugins = stage._load_plugins()

        stage.plugins[0].bom_access = BomAccess.FULL_ACCESS
        stage.plugins[1].bom_access = BomAccess.COMPONENT_ACCESS

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Stage test-stage has one or more plugins with FULL_ACCESS: UnitTestPlugin, and multiple plugins defined for the stage.\n    Any plugin with FULL BOM access must be the only plugin in the stage"

    @patch(
        'hoppr.processor.plugin_instance',
        side_effect=[
            UnitTestPlugin(
                Context(
                    Manifest(),
                    "ROOT",
                    None,
                    None,
                    3,
                    logfile_lock=multiprocessing.Manager().RLock(),
                )
            ),
            SecondPlugin(
                Context(
                    Manifest(),
                    "ROOT",
                    None,
                    None,
                    3,
                    logfile_lock=multiprocessing.Manager().RLock(),
                )
            ),
        ],
    )
    @patch.object(Manifest, 'load_file')
    def test_check_bom_access_component(self, mock_manifest_load, mock_plugin_instance):
        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None),
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        stage.plugins = stage._load_plugins()

        stage.plugins[0].bom_access = BomAccess.COMPONENT_ACCESS
        stage.plugins[1].bom_access = BomAccess.COMPONENT_ACCESS

        result = stage._check_bom_access()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "Stage test-stage has one or more plugins with COMPONENT_ACCESS: UnitTestPlugin, SecondPlugin, and required component coverage for the stage of OPTIONAL.\n    If any plugins have COMPONENT access, the stage required coverage must be EXACTLY_ONCE or NO_MORE_THAN_ONCE."

    @patch(
        'hoppr.processor.plugin_instance',
        return_value=UnitTestPlugin(
            Context(
                Manifest(),
                "ROOT",
                None,
                None,
                3,
                logfile_lock=multiprocessing.Manager().RLock(),
            )
        ),
    )
    @patch.object(Manifest, 'load_file')
    def test_stage_run_success(self, mock_manifest_load, mock_plugin_instance):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'post_stage_process' processes returned 'retry'"

    @patch('hoppr.processor.plugin_instance', side_effect = ModuleNotFoundError())
    @patch.object(Manifest, 'load_file')
    def test_stage_run_module_not_found(self, mock_manifest_load, mock_plugin_instance):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @patch.object(
        StageProcessor, '_check_bom_access', return_value=Result.fail("Mock Failure")
    )
    @patch(
        'hoppr.processor.plugin_instance',
        return_value=UnitTestPlugin(
            Context(
                Manifest(),
                "ROOT",
                None,
                None,
                3,
                logfile_lock=multiprocessing.Manager().RLock(),
            )
        ),
    )
    @patch.object(Manifest, 'load_file')
    def test_stage_run_bad_bom_coverage(self, mock_manifest_load, mock_plugin_instance, mock_check_bom_access):
        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"

    @patch.object(Future, 'result', return_value = Result.fail())
    @patch('hoppr.processor.plugin_instance', return_value = UnitTestPlugin(None))
    @patch.object(Manifest, 'load_file')
    def test_stage_run_all_fail(self, mock_manifest_load, mock_plugin_instance, mock_future_result):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'pre_stage_process' processes failed\n2 'process_component' processes failed\n1 'post_stage_process' processes failed"

    @patch.object(Future, 'result', side_effect = [
        Result.success(),
        Result.retry(), Result.fail(),
        Result.success(),
    ])
    @patch('hoppr.processor.plugin_instance', return_value = UnitTestPlugin(None))
    @patch.object(Manifest, 'load_file')
    def test_stage_run_fail_and_retry(self, mock_manifest_load, mock_plugin_instance, mock_future_result):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        stage = StageProcessor(StageRef("test-stage", {}), context)
        stage.context.delivered_sbom.components = test_component_list

        stage.plugin_ref_list = [
            Plugin(name="test.test_process.UnitTestHelloPlugin", config=None)
        ]

        result = stage.run()
        assert result.is_fail(), f"Expected FAIL result, got {result}"
        assert result.message == "1 'process_component' processes failed, and 1 returned 'retry'\n"

    @patch("shutil.copyfile")
    @patch.object(Manifest, 'load_file')
    def test_collect_file(self, mock_manifest_load, mock_copyfile):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)
        proc.context = context
        proc.logger = MemoryLogger("hoppr3.log", lock=context.logfile_lock, log_name="test_logger", flush_immed=True)

        proc._collect_file("/path/to/metadata_file_name", "target_dir/")

        mock_copyfile.assert_called_once_with("/path/to/metadata_file_name", "target_dir/%2Fpath%2Fto%2Fmetadata_file_name")
        proc.logger.close()
        os.remove("hoppr3.log")

    @patch("hoppr.processor.download_file")
    @patch.object(Credentials, "find_credentials", return_value=CredObject("un", "pw"))
    @patch.object(Manifest, 'load_file')
    def test_collect_url(self, mock_manifest_load, mock_find_creds, mock_downloadfile):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)
        proc.context = context
        proc.logger = MemoryLogger("hoppr4.log", lock=context.logfile_lock, log_name="test_logger", flush_immed=True)

        proc._collect_url("http://metadata_url", "target_dir/")

        mock_downloadfile.assert_called_once_with("http://metadata_url", "target_dir/http%3A%2F%2Fmetadata_url", None)
        proc.logger.close()
        os.remove("hoppr4.log")

    @patch("hoppr.oci_artifacts.pull_artifact_to_disk")
    @patch.object(Credentials, "find_credentials", return_value=CredObject("un", "pw"))
    @patch.object(Manifest, 'load_file')
    def test_collect_oci_artifact(self, mock_manifest_load, mock_find_creds, mock_downloadfile):
        transfer = Transfer.load_file(Path("test/resources/transfer/transfer-test.yml"))
        manifest = Manifest.load_file("mock_manifest_file")

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)
        proc.context = context
        proc.logger = MemoryLogger("hoppr4.log", lock=context.logfile_lock, log_name="test_logger", flush_immed=True)

        artifact = "registry.test.com/my/repo/image:1.2.3"
        proc._collect_oci_artifact(artifact, "target_dir/")

        mock_downloadfile.assert_called_once_with(artifact, "target_dir/registry.test.com%2Fmy%2Frepo%2Fimage%3A1.2.3", allow_version_discovery=True)
        proc.logger.close()
        os.remove("hoppr4.log")


    @staticmethod
    @patch.object(Manifest, "load_sbom", return_value=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"))
    def manifest_from_string(man_string, parent=None):
        manifest = Manifest()
        input_dict = utils.load_string(man_string)
        manifest = Manifest()
        manifest.populate(input_dict, parent)
        return manifest

    parent_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test parent Manifest"
  version: 0.1.0
  description: "test parent manifest"

sboms: []

includes:
  - local: test/child-manifest.yml
  - url: http://dummy/empty_manifest.yml

repositories:
  generic:
    - url: https://media3.giphy.com/
"""

    child_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test Child Manifest"
  version: 0.1.0
  description: "Test Child Manifest"

sboms:
  - local: test/bom.json
  - url: http://dummy/bom2.json
  - oci: registry.test.com/my/repo/image:1.2.3


includes: []

repositories: {}
"""

    empty_manifest = """
schemaVersion: v1
kind: Manifest

metadata:
  name: "Test Empty Manifest"
  version: 0.1.0
  description: "Test Empty Manifest"

sboms: []

includes: []

repositories: {}
"""
    @patch.object(Manifest, "load_file", return_value=manifest_from_string(child_manifest))
    @patch.object(Manifest, "load_url", return_value=manifest_from_string(empty_manifest))
    @patch.object(HopprProcessor, "_collect_file")
    @patch.object(HopprProcessor, "_collect_url")
    @patch.object(HopprProcessor, "_collect_oci_artifact")
    def test_collect_manifest_metadata(self, mock_collect_url, mock_collect_file, mock_man_from_url, mock_man_from_file, mock_man_from_oci):
        transfer = Transfer.load_file(Path("test", "resources", "transfer", "transfer-test.yml"))
        manifest = self.manifest_from_string(self.parent_manifest)

        context = Context(
            manifest=manifest,
            collect_root_dir="ROOT",
            consolidated_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            delivered_sbom=Bom(specVersion="1.4", version=1, bomFormat="CycloneDX"),
            max_processes=3,
            logfile_lock=multiprocessing.Manager().RLock(),
        )

        proc = HopprProcessor(transfer, manifest)

        proc._collect_manifest_metadata(manifest, "target_dir/")
