"""
Test module for CollectPypiPlugin class
"""

# pylint: disable=protected-access
# pylint: disable=redefined-outer-name
# pylint: disable=unused-argument

import importlib.util

from pathlib import Path
from subprocess import CompletedProcess

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.configs.credentials import Credentials
from hoppr.core_plugins.collect_pypi_plugin import CollectPypiPlugin
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.hoppr_types.manifest_file_content import Repository
from hoppr.hoppr_types.purl_type import PurlType
from hoppr.result import Result


@pytest.fixture(name="component")
def component_fixture():
    """
    Test Component fixture
    """
    return Component(name="TestComponent", purl="pkg:pypi/hoppr/hippo@1.2.3", type="file")  # type: ignore


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectPypiPlugin)])
def plugin_fixture(plugin_fixture: CollectPypiPlugin, monkeypatch: MonkeyPatch, tmp_path: Path) -> CollectPypiPlugin:
    """
    Override and parametrize plugin_fixture to return CollectPypiPlugin
    """
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(
        target=plugin_fixture, name="_get_repos", value=lambda comp: ["https://pypi.hoppr.com/hoppr/pypi"]
    )

    plugin_fixture.context.collect_root_dir = str(tmp_path)
    plugin_fixture.context.manifest.consolidated_repositories = {
        PurlType.RPM: [Repository(url="https://somewhere.com", description="")]
    }

    return plugin_fixture


def test_collect_pypi_repository_url_fail(
    plugin_fixture: CollectPypiPlugin,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=plugin_fixture, name="check_purl_specified_url", value=Result.fail)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"


def test_collect_pypi_success(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: success
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[dict(returncode=1)], indirect=True)
def test_collect_pypi_fail(
    plugin_fixture: CollectPypiPlugin,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredObject,
    component: Component,
    monkeypatch: MonkeyPatch,
):
    """
    Test collect method: fail
    """
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)
    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to download")


def test_collect_pypi_pip_not_found(plugin_fixture: CollectPypiPlugin, component: Component, monkeypatch: MonkeyPatch):
    """
    Test collect method: pip not found
    """
    monkeypatch.setattr(target=importlib.util, name="find_spec", value=lambda *args, **kwargs: None)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail()
    assert collect_result.message == "The pip package was not found. Please install and try again."


def test_get_version(plugin_fixture: CollectPypiPlugin):
    """
    Test get_version method
    """
    assert len(plugin_fixture.get_version()) > 0
