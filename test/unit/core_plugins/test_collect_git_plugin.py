"""
Test module for CollectGitPlugin class
"""
# pylint: disable=redefined-outer-name,too-many-arguments,unused-argument,duplicate-code

from subprocess import CompletedProcess
from typing import Dict, List

import pytest

from hoppr_cyclonedx_models.cyclonedx_1_4 import Component
from pytest import MonkeyPatch

import hoppr.plugin_utils

from hoppr.configs.credentials import Credentials
from hoppr.core_plugins.collect_git_plugin import CollectGitPlugin
from hoppr.hoppr_types.cred_object import CredObject
from hoppr.result import Result, ResultStatus


@pytest.fixture(scope="function", params=[dict(plugin_class=CollectGitPlugin)])
def plugin_fixture(plugin_fixture: CollectGitPlugin) -> CollectGitPlugin:
    """
    Override and parametrize plugin_fixture to return CollectGitPlugin
    """

    return plugin_fixture


@pytest.fixture(name="component")
def component_fixture() -> Component:
    """
    Test Component fixture
    """

    return Component(name="TestComponent", purl="pkg:git/something/else@1.2.3", type="file")  # type: ignore


def get_repos(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos method
    """

    return ["http://somewhere.com", "https://somewhere.com"]


def get_ssh_repo(*args, **kwargs) -> List[str]:
    """
    Mock _get_repos_method but instead return with an ssh scheme
    """

    return ["ssh://somewhere.com"]


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector
    """

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collect_git_fail(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    run_command_fixture: CompletedProcess,
    find_credentials_fixture: CredObject,
):
    """
    Test a failing run of the Git Collector
    """

    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith("Failure after 3 attempts, final message Failed to clone")


@pytest.mark.parametrize(argnames="completed_process_fixture", argvalues=[{"returncode": 1}], indirect=True)
def test_collector_git_update_fail(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    find_credentials_fixture: CredObject,
    run_command_fixture: CompletedProcess,
):
    """
    Test git collector is able to clone but not able to commit changes
    """

    def mock_clone(*args, **kwargs):
        return Result(status=ResultStatus(0))

    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_ssh_repo)
    monkeypatch.setattr(target=plugin_fixture, name="git_clone", value=mock_clone)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message.startswith(
        "Failure after 3 attempts, final message Failed to make the clone usable as a remote"
    )


def test_get_version(plugin_fixture: CollectGitPlugin):
    """
    Test git collector get version method
    """
    assert len(plugin_fixture.get_version()) > 0


def test_collector_git_command_not_found(
    plugin_fixture: CollectGitPlugin, component: Component, monkeypatch: MonkeyPatch
):
    """
    Test git collector with missing required command
    """

    def mock_bad_result(*args, **kwargs):
        return Result.fail("[mock] command not found")

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=mock_bad_result)
    collect_result = plugin_fixture.process_component(component)
    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"
    assert collect_result.message == "[mock] command not found"


def test_collect_git_url_mismatch(plugin_fixture: CollectGitPlugin, monkeypatch: MonkeyPatch):
    """
    Test git collector with mismatched urls from the repo and the component
    """

    bad_comp = Component(
        name="TestComponent",
        purl="pkg:git/something/else@1.2.3?repository_url=my.repo",
        type="file",  # type: ignore
    )

    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_repos)
    collect_result = plugin_fixture.process_component(bad_comp)

    assert collect_result.is_fail(), f"Expected FAIL result, got {collect_result}"

    assert collect_result.message == (
        "Purl-specified repository url (my.repo) does not match current repo (https://somewhere.com)."
    )


@pytest.mark.parametrize(
    argnames=["config_fixture", "completed_process_fixture"],
    argvalues=[(dict(git_command="git"), dict(returncode=0))],
    indirect=True,
)
def test_collect_git_ssh_success(
    plugin_fixture: CollectGitPlugin,
    monkeypatch: MonkeyPatch,
    component: Component,
    config_fixture: Dict[str, str],
    find_credentials_fixture: CredObject,
    run_command_fixture: CompletedProcess,
):
    """
    Test a successful run of the Git Collector with ssh
    """

    # pylint: disable=too-many-arguments
    monkeypatch.setattr(target=Credentials, name="find_credentials", value=find_credentials_fixture)
    monkeypatch.setattr(target=hoppr.plugin_utils, name="check_for_missing_commands", value=Result.success)
    monkeypatch.setattr(target=plugin_fixture, name="_get_repos", value=get_ssh_repo)
    monkeypatch.setattr(target=plugin_fixture, name="run_command", value=run_command_fixture)

    collect_result = plugin_fixture.process_component(component)

    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
    assert collect_result.is_success(), f"Expected SUCCESS result, got {collect_result}"
